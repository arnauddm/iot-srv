package config

import(
	"log"
	"database/sql"
	"github.com/go-sql-driver/mysql"
)

var db *sql.DB

func InitDatabase() {
	var err error

    dbConfig := mysql.NewConfig()
    dbConfig.User = "root"
    dbConfig.Passwd = "root"
    dbConfig.Addr = "localhost:3306" // database : name of the container
    dbConfig.DBName = "notify"
    dbConfig.Net = "tcp"
		dbConfig.ParseTime = true

    db, err = sql.Open("mysql", dbConfig.FormatDSN())
    if err != nil {
        panic(err)
    }

	CreateUsersTable()
}

func Db() *sql.DB {
	return db
}

func CreateUsersTable() {
	_, err := db.Exec("CREATE TABLE IF NOT EXISTS users(id serial, email varchar(255), password varchar(255), token varchar(255), created_at datetime, updated_at datetime, constraint pk primary key(id))")

	if err != nil {
		log.Fatal(err)
	}
}
