package controllers

import(
	"notify-srv/models"
	"notify-srv/utils"
	"notify-srv/types"

	"encoding/json"
	"github.com/gorilla/mux"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
)

func AllUsers(w http.ResponseWriter, r *http.Request) {
	utils.SetUpResponseWriter(&w)

	utils.GetUserTokenFromHeader(r)

	json.NewEncoder(w).Encode(models.AllUsers())
}

func Login(w http.ResponseWriter, r *http.Request) {
	utils.SetUpResponseWriter(&w)

	decoder := json.NewDecoder(r.Body)

	credentials := struct {
    	Email string `json:"email"`
    	Password string `json:"password"`
	}{}

	err := decoder.Decode(&credentials)

	if err != nil {
		log.Fatal("[UsersToken] ", err)
	}

	token := models.UserToken(credentials.Email, credentials.Password)

	if strings.Count(token, "") == 1 {
		var err types.Error
		err.Error = "No user found..."
		json.NewEncoder(w).Encode(err)
	} else {
		tokenStruct := struct {
			Token string `json:"token"`
		}{}

		tokenStruct.Token = token
		json.NewEncoder(w).Encode(tokenStruct)
	}
}

func NewUser(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json;charset=UTF-8")
	w.WriteHeader(http.StatusOK)

	body, err := ioutil.ReadAll(r.Body)

	if err != nil {
		log.Fatal("[UsersCreate] ", err)
	}

	var user models.User

	err = json.Unmarshal(body, &user)

	if err != nil {
		log.Fatal("[UsersCreate] ", err)
	}

	models.NewUser(&user)

	json.NewEncoder(w).Encode(user)
}

func UsersDelete(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json;charset=UTF-8")
	w.WriteHeader(http.StatusOK)

	vars := mux.Vars(r)

	token := vars["token"]

	err := models.DeleteUserByToken(token)

	if err != nil {
		log.Fatal("[UsersDelete] ", err)
	}
}
