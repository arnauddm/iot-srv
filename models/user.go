package models

import (
	"notify-srv/config"
	"notify-srv/utils"

	"log"
	"time"
	"golang.org/x/crypto/bcrypt"
)

type User struct {
	Id			int `json:"id"`
	Email		string `json:"email"`
	Password	string `json:"password"`
	Token		string `json:"token"`
	CreatedAt	time.Time `json:"created_at"`
	UpdatedAt	time.Time `json:"updated_at"`
}

type Users []User

func NewUser(u *User) {
	if u == nil {
		log.Fatal("[NewUser] ", u)
	}

	u.CreatedAt = time.Now()
	u.UpdatedAt = time.Now()
	u.Token = utils.TokenGenerator()
	hashed, err := bcrypt.GenerateFromPassword([]byte(u.Password), 10)

	if err != nil {
		log.Fatal("Error while hashing user password...")
	}

	u.Password = string(hashed)

	res, err := config.Db().Exec("INSERT INTO users (email, password, token, created_at, updated_at) VALUES (?, ?, ?, ?, ?);", u.Email, u.Password, u.Token, u.CreatedAt, u.UpdatedAt)

	if err != nil {
		log.Fatal("[NewUser] ", err)
	} else {
		id, err := res.LastInsertId()

		if err != nil {
			log.Fatal("[NewUser] ", err)
		} else {
			u.Id = int(id)
		}
	}
}

func AllUsers() *Users {
	var users Users

	rows, err := config.Db().Query("SELECT * FROM users;")

	if err != nil {
		log.Fatal("[AllUsers] ", err)
	}

	// Close rows after all readed
	defer rows.Close()

	for rows.Next() {
		var u User

		err := rows.Scan(&u.Id, &u.Email, &u.Password, &u.Token, &u.CreatedAt, &u.UpdatedAt)

		if err != nil {
			log.Fatal("[AllUsers] ", err)
		}

		users = append(users, u)
	}

	return &users
}

func UpdateUser(user *User) {
	user.UpdatedAt = time.Now()

	stmt, err := config.Db().Prepare("UPDATE users SET email=$1, password=$2, updated_at=$3 WHERE token=$4;")

	if err != nil {
		log.Fatal("[UpdateUser] ", err)
	}

	_, err = stmt.Exec(user.Email, user.Password, user.UpdatedAt, user.Token)

	if err != nil {
		log.Fatal("[UpdateUser] ", err)
	}
}

func DeleteUserByToken(token string) error {
	_, err := config.Db().Exec("DELETE FROM users WHERE token=?;", token)

	if err != nil {
		log.Fatal("[DeleteUser] ", err)
	}

	return err
}

func FindUserByToken(token string) *User {
	var user User

	row := config.Db().QueryRow("SELECT * FROM users WHERE token=?;", token)

	if row == nil {
		log.Fatal("[FindUserByToken] Empty row")
	}

	err := row.Scan(&user.Id, &user.Email, &user.Password, &user.Token, &user.UpdatedAt, &user.CreatedAt)

	if err != nil {
		log.Fatal("[FindUserByToken] ", err)
	}

	return &user
}

func UserToken(email string, password string) string {
	var token string
	var hashed string
	row := config.Db().QueryRow("SELECT token, password FROM users WHERE email=?;", email)

	if row == nil {
		log.Fatal("[UserToken] No row...")
	}

	row.Scan(&token, &hashed)

	if bcrypt.CompareHashAndPassword([]byte(hashed), []byte(password)) != nil {
		return ""
	}
	return token
}
