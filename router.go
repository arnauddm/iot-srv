package main

import(
	"notify-srv/controllers"

	"github.com/gorilla/mux"
)

func InitializeRouter() *mux.Router {
	router := mux.NewRouter().StrictSlash(true)

	router.Methods("GET").Path("/users").Name("Index").HandlerFunc(controllers.AllUsers)
	router.Methods("POST").Path("/login").Name("Login").HandlerFunc(controllers.Login)
	router.Methods("POST").Path("/user").Name("Create").HandlerFunc(controllers.NewUser)
	router.Methods("DELETE").Path("/user/{id}").Name("Delete").HandlerFunc(controllers.UsersDelete)

	return router
}
