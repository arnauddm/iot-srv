package main

import(
	"notify-srv/config"
	
	"log"
	"net/http"
	"fmt"
)

func main() {
	fmt.Println("Starting Notify server on port 8080...")

	config.InitDatabase()
	router := InitializeRouter()

	log.Fatal(http.ListenAndServe(":8080", router))
}
