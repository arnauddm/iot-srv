package utils

import(
	"crypto/rand"
	"fmt"
	"net/http"
	//"models"
)

func TokenGenerator() string {
	b := make([]byte, 32)
	rand.Read(b)
	return fmt.Sprintf("%x", b)
}

func SetUpResponseWriter(w *http.ResponseWriter) {
	(*w).Header().Set("Content-Type", "application/json;charset=UTF-8")
	(*w).WriteHeader(http.StatusOK)
}

func GetUserTokenFromHeader(r *http.Request) string {
	user_token := r.Header.Get("Authorization")
	return user_token
}
